package urm.com.urm_manager.main_activity;

import urm.com.urm_manager.models.ParkingSession;

/*------------------------------------------------------------------------------
 - Author: Jiaxin Li - Co-Founder & Project Manager @ URM
 - Co-Author: Soren Netka - Co-Founder & Senior Engineer @URM
 -                    Priya G. K. - Co-Founder & Senior Engineer @URM
 - Copyright (c) 2018. All rights reserved.
 -----------------------------------------------------------------------------*/

public interface RecyclerItemClickListener {
    void onItemClick(ParkingSession session);
}
