package urm.com.urm_manager.main_activity;

import java.util.ArrayList;

import urm.com.urm_manager.models.ParkingSession;

/*------------------------------------------------------------------------------
 - Author: Jiaxin Li - Co-Founder & Project Manager @ URM
 - Co-Author: Soren Netka - Co-Founder & Senior Engineer @URM
 -                    Priya G. K. - Co-Founder & Senior Engineer @URM
 - Copyright (c) 2018. All rights reserved.
 -----------------------------------------------------------------------------*/

public class MainPresenter implements MainContract.presenter, MainContract.GetSessionIntractor.OnFinishedListener {

    private MainContract.MainView mainView;
    private MainContract.GetSessionIntractor getNoticeIntractor;

    public MainPresenter(MainContract.MainView mainView, MainContract.GetSessionIntractor getNoticeIntractor) {
        this.mainView = mainView;
        this.getNoticeIntractor = getNoticeIntractor;
    }

    @Override
    public void onDestroy() {

        mainView = null;

    }

    @Override
    public void onRefreshButtonClick() {

        if(mainView != null){
            mainView.showProgress();
        }
        getNoticeIntractor.getSessionArrayList(this);

    }

    @Override
    public void requestDataFromServer() {
        getNoticeIntractor.getSessionArrayList(this);
    }


    @Override
    public void onFinished(ArrayList<ParkingSession> noticeArrayList) {
        if(mainView != null){
            mainView.setDataToRecyclerView(noticeArrayList);
            mainView.hideProgress();
        }
    }

    @Override
    public void onFailure(Throwable t) {
        if(mainView != null){
            mainView.onResponseFailure(t);
            mainView.hideProgress();
        }
    }
}