package urm.com.urm_manager.main_activity;

import urm.com.urm_manager.models.ParkingSession;
import java.util.ArrayList;

/*------------------------------------------------------------------------------
 - Author: Jiaxin Li - Co-Founder & Project Manager @ URM
 - Co-Author: Soren Netka - Co-Founder & Senior Engineer @URM
 -                    Priya G. K. - Co-Founder & Senior Engineer @URM
 - Copyright (c) 2018. All rights reserved.
 -----------------------------------------------------------------------------*/

public interface MainContract {
    /**
     * Call when user interact with the view and other when view OnDestroy()
     * */
    interface presenter{

        void onDestroy();

        void onRefreshButtonClick();

        void requestDataFromServer();

    }

    /**
     * showProgress() and hideProgress() would be used for displaying and hiding the progressBar
     * while the setDataToRecyclerView and onResponseFailure is fetched from the GetNoticeInteractorImpl class
     **/
    interface MainView {

        void showProgress();

        void hideProgress();

        void setDataToRecyclerView(ArrayList<ParkingSession> noticeArrayList);

        void onResponseFailure(Throwable throwable);

    }

    /**
     * Intractors are classes built for fetching data from our database, web services, or any other data source.
     **/
    interface GetSessionIntractor {

        interface OnFinishedListener {
            void onFinished(ArrayList<ParkingSession> sessionArrayList);
            void onFailure(Throwable t);
        }

        void getSessionArrayList(OnFinishedListener onFinishedListener);
    }
}
