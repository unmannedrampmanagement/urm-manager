package urm.com.urm_manager.models;

import com.google.gson.annotations.SerializedName;

/*------------------------------------------------------------------------------
 - Author: Jiaxin Li - Co-Founder & Project Manager @ URM
 - Co-Author: Soren Netka - Co-Founder & Senior Engineer @URM
 -                    Priya G. K. - Co-Founder & Senior Engineer @URM
 - Copyright (c) 2018. All rights reserved.
 -----------------------------------------------------------------------------*/

public class ParkingSession {
    @SerializedName("License_Plate")
    private String licensePlate;
    @SerializedName("time_In")
    private String timeIn;

    public ParkingSession(String licensePlate, String timeIn) {
        this.licensePlate = licensePlate;
        this.timeIn = timeIn;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getTimeIn() {
        return timeIn;
    }

    public void setTitle(String timeIn) {
        this.timeIn = timeIn;
    }

}
