package urm.com.urm_manager.my_interface;

import retrofit2.Call;
import retrofit2.http.GET;
import urm.com.urm_manager.models.ParkingSession;
import urm.com.urm_manager.models.ParkingSessionList;

/*------------------------------------------------------------------------------
 - Author: Jiaxin Li - Co-Founder & Project Manager @ URM
 - Co-Author: Soren Netka - Co-Founder & Senior Engineer @URM
 -                    Priya G. K. - Co-Founder & Senior Engineer @URM
 - Copyright (c) 2018. All rights reserved.
 -----------------------------------------------------------------------------*/

public interface APIInterface {

    @GET("getTest/parking-sessions")
    Call<ParkingSessionList> getSessionData();

}
