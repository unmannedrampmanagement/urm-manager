package urm.com.urm_manager.adapters;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import urm.com.urm_manager.main_activity.RecyclerItemClickListener;
import urm.com.urm_manager.models.ParkingSession;
import urm.com.urm_manager.R;

/*------------------------------------------------------------------------------
 - Author: Jiaxin Li - Co-Founder & Project Manager @ URM
 - Co-Author: Soren Netka - Co-Founder & Senior Engineer @URM
 -            Priya G. K. - Co-Founder & Senior Engineer @URM
 - Copyright (c) 2018. All rights reserved.
 -----------------------------------------------------------------------------*/

public class SessionAdapter extends RecyclerView.Adapter<SessionAdapter.SessionViewHolder> {
    private ArrayList<ParkingSession> dataList;
    private RecyclerItemClickListener recyclerItemClickListener;

    public SessionAdapter(ArrayList<ParkingSession> dataList, RecyclerItemClickListener recyclerItemClickListener) {
        this.dataList = dataList;
        this.recyclerItemClickListener = recyclerItemClickListener;
    }


    @Override
    public SessionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.single_view_row, parent, false);
        return new SessionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SessionViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        holder.txtNoticeTitle.setText(dataList.get(position).getLicensePlate());
        //holder.txtNoticeBrief.setText(dataList.get(position).getBrief());
        //holder.txtNoticeFilePath.setText(dataList.get(position).getFileSource());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerItemClickListener.onItemClick(dataList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class SessionViewHolder extends RecyclerView.ViewHolder {

        TextView txtNoticeTitle, txtNoticeBrief, txtNoticeFilePath;

        SessionViewHolder(View itemView) {
            super(itemView);
            txtNoticeTitle = itemView.findViewById(R.id.txt_notice_title);
            txtNoticeBrief = itemView.findViewById(R.id.txt_notice_brief);
            txtNoticeFilePath = itemView.findViewById(R.id.txt_notice_file_path);

        }
    }
}