package urm.com.urm_manager.models;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;

/*------------------------------------------------------------------------------
 - Author: Jiaxin Li - Co-Founder & Project Manager @ URM
 - Co-Author: Soren Netka - Co-Founder & Senior Engineer @URM
 -                    Priya G. K. - Co-Founder & Senior Engineer @URM
 - Copyright (c) 2018. All rights reserved.
 -----------------------------------------------------------------------------*/

public class ParkingSessionList {
    @SerializedName("body")
    private ArrayList<ParkingSession> sessionList;

    public ArrayList<ParkingSession> getSessionArrayList() {
        return sessionList;
    }

    public void setSessionArrayList(ArrayList<ParkingSession> sessionArrayList) {
        this.sessionList = sessionArrayList;
    }
}
