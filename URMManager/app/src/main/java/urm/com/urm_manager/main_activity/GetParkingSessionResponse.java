package urm.com.urm_manager.main_activity;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import urm.com.urm_manager.network.APIClient;
import urm.com.urm_manager.my_interface.APIInterface;
import urm.com.urm_manager.models.ParkingSessionList;

/*------------------------------------------------------------------------------
 - Author: Jiaxin Li - Co-Founder & Project Manager @ URM
 - Co-Author: Soren Netka - Co-Founder & Senior Engineer @URM
 -                    Priya G. K. - Co-Founder & Senior Engineer @URM
 - Copyright (c) 2018. All rights reserved.
 -----------------------------------------------------------------------------*/

public class GetParkingSessionResponse implements MainContract.GetSessionIntractor {

    @Override
    public void getSessionArrayList(final OnFinishedListener onFinishedListener) {


        /** Create handle for the RetrofitInstance interface*/
        APIInterface service = APIClient.getRetrofitInstance().create(APIInterface.class);

        /** Call the method with parameter in the interface to get the notice data*/
        Call<ParkingSessionList> call = service.getSessionData();

        /**Log the URL called*/
        Log.wtf("URL Called", call.request().url() + "");

        call.enqueue(new Callback<ParkingSessionList>() {
            @Override
            public void onResponse(Call<ParkingSessionList> call, Response<ParkingSessionList> response) {
                onFinishedListener.onFinished(response.body().getSessionArrayList());

            }

            @Override
            public void onFailure(Call<ParkingSessionList> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });

    }
}